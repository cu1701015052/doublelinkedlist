public class Node {

  int data;

  public Node next;
  public Node previous; // USED IN DOUBLE LINKED LIST

  public Node() {
    this.data = 0;
  }
  
  public Node(int data) {
    this.data = data;
  }
  
//  protected void finalize() throws Throwable {
//    System.out.print( data );
//    System.out.print(" released \n");
//
//    super.finalize();
//  }
  
  public String description() {
    String text = Integer.toString(data);
    
    if( next != null ) {
      text = text + " -> " + next.description();
    }
    return text;
  }
  

  public Node searchNodeByDataRecursively(int dataToSearch) {
    
    if( this.data == dataToSearch ) {
      return this;
    }
    
    if( next == null ) {
      return null;
    }
    return next.searchNodeByDataRecursively(dataToSearch);
  }
  
}


