public class SinglyLinkedList {
  
  public Node top;
  
  public void add(Node next) {
    if( top == null ) {
      top = next;
      top.next = null;
      return;
    }
    
    Node node = top;
    while( node.next != null ) {
      node = node.next;
    }
    
    node.next = next;
    next.next = null;
  }
  
  public void set(Node node) {
    top = node;
  }
  
  // ADD node AT index
  public boolean insert(Node addingNode, int index) {
    if( index < 0 ) { return false; }
    if( top == null ) {
      add(addingNode);
      return true;
    }
    
    if( index == 0 ) {
      addingNode.next = top;
      top = addingNode;
      return true;
    }
    
    Node node = getNodeAt(index-1);
    if( node == null || node.next == null) {
      add(addingNode);
      return true;
    }
    
    addingNode.next = node.next;
    node.next = addingNode;
    return true;
  }
  
  // DELETE node AT index AND RETURN THE DELETED NODE
  public Node delete(int index) {
    if( index < 0 ) { return null; }
    
    if( index == 0 ) {
      Node deletedNode = top;
      top = top.next;
      deletedNode.next = null;

      return deletedNode;
    }
    
    Node node = getNodeAt(index-1);
    if( node == null || node.next == null ) {
      return null;
    }
    
    Node deletedNode = node.next;
    node.next = node.next.next;
    deletedNode.next = null;
    return deletedNode;
  }
  
  public void delete(Node obj) {
    delete(search(obj));
  }
  
  public int search(Node obj) {
    
    Node node = top;
    int index = 0;
    while(node != null) {
      if(node == obj) {
        return index;
      }
      
      node = node.next;
      index += 1;
    }
    
    return -1;
  }
  
  public Node searchNodeByData(int dataToSearch) {
    
    Node node = top;
    
    while(node != null) {
      if(node.data == dataToSearch) {
        return node;
      }
      
      node = node.next;
    }
    
    return null;
  }
  
  
  public Node searchNodeByDataRecursively(int dataToSearch) {
    return top.searchNodeByDataRecursively(dataToSearch);
  }

  
  protected Node getNodeAt(int index) {
    if( index < 0 ) { return null; }
    
    Node node = top;
    
    if(index == 0) {
      return node;
    }
    
    while(index>0) {
      node = node.next;
      if( node == null ) {
        return null;
      }
      
      index--;
      if(index == 0) {
        return node;
      }
    }
    
    return null;
  }

  public String description() {
    if( top == null ) { return null; }
    return top.description();
  }
  
  public int count() {
    int count = 0;
    Node obj = top;
    while(obj != null) {
      count ++;
      obj = obj.next;
    }
    
    return count;
  }
  
  public boolean check() {
    if( top == null ){
      System.out.println("check: null");
      return false;
    }
    
    if( top.next == null ){
      System.out.println("check: single node");
      return true;
    }
    
    return true;
  }
  
}

