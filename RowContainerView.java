import java.awt.*;
import java.util.*;
import java.lang.*;

public class RowContainerView {
  
  public void reload() {
  }
  
  Node focused = null;
  
  public Node hitTest(int x, int y) {
    focused = null;
    
    CoordinateTransformer transformer = CoordinateTransformer.getInstance();
    
    int myY = (int)transformer.viewY;
    
    if( myY <= y && y < myY + transformer.rowHeight ) {
      DoublyLinkedList nodeList = DataSource.getInstance().activeList;
      
      Node obj = nodeList.top;
      
      while( obj != null ) {
        NodeView view = new NodeView(obj);
        
        if( view.hitTest(x, y) == true ) {
          focused = obj;
        } else {
        }
        
        obj = obj.next;
      }
    }else {
      focused = null;
    }
    return focused;
  }
  
  public void draw(Graphics g) {
    CoordinateTransformer transformer = CoordinateTransformer.getInstance();
    
    // DRAW BACKGROUND
    g.setColor(new Color(0,0,0,10));
    
    float dx = (transformer.rightX - transformer.leftX);
    
    g.fillRect((int)transformer.leftX, (int)transformer.viewY, (int)dx, transformer.rowHeight);


    // DRAW BOXES

    DoublyLinkedList nodeList = DataSource.getInstance().activeList;
    
    Node obj = nodeList.top;
    NodeView previousView = null;

    while( obj != null ) {
      NodeView view = new NodeView(obj);
      if( focused == obj ) {
        view.focused = true;
      }
      
      view.draw(g);
      
      obj = obj.next;
      previousView = view;

    }
  }
}
