#!/bin/sh

#  run.sh
#
# ソースの追加の仕方 File -> New -> File... で空のファイルを選択。拡張子を.javaにしておくこと
#
# 実行の仕方　Sample_01_3 を実行したいクラス名に書き換えて、⌘R（またはRunボタン"▶"）
#
# 標準入出力は自動的に起動したターミナルのなかで。

#java -classpath $JAVA_SOURCE_PATH $CLASS_FILE

CLASSNAME="Project_1701015052"

bar="java -classpath "$JAVA_SOURCE_PATH" "$CLASSNAME

osascript -e "tell application \"Terminal\" to do script \"$bar\""
