import java.util.*;
import java.awt.*;

public class NodeView {
  Node node;
  boolean focused = false;

  public NodeView(Node node) {
    this.node = node;
  }
  
  public boolean hitTest(int x, int y) {
    
    CoordinateTransformer transformer = CoordinateTransformer.getInstance();
    
    int myStartX = transformer.xFromData(node.data);
    int myEndX = transformer.xFromData(node.data+1);
    
    if( myStartX <= x  && x < myEndX )
      return true;
    
    return false;
  }
  
  public int getMinX() {
    CoordinateTransformer transformer = CoordinateTransformer.getInstance();
    DataSource dataSource = DataSource.getInstance();
    
    int y = (int)transformer.viewY;
    int startX = transformer.xFromData(node.data);

    return startX;
  }
  
  public int getMaxX() {
    CoordinateTransformer transformer = CoordinateTransformer.getInstance();
    DataSource dataSource = DataSource.getInstance();
    
    int y = (int)transformer.viewY;
    int startX = transformer.xFromData(node.data);
    int endX = transformer.xFromData(node.data+1);
    
    return endX;
  }
  
  public int getMidY() {
    CoordinateTransformer transformer = CoordinateTransformer.getInstance();
    DataSource dataSource = DataSource.getInstance();
    
    int y = (int)transformer.viewY;
    int height = transformer.rowHeight;

    return y + height/2;
  }
  
  public void draw(Graphics g) {
    CoordinateTransformer transformer = CoordinateTransformer.getInstance();
    DataSource dataSource = DataSource.getInstance();

    int y = (int)transformer.viewY;
    int startX = transformer.xFromData(node.data);
    int endX = transformer.xFromData(node.data+1);
    int height = transformer.rowHeight;
    
    startX += 1;
    endX -= 1;
    
    g.setColor(new Color(84,171,242));
    g.fillRect(startX, y, endX - startX, height);
    if( focused ) {
      g.setColor(new Color(0,0,0,80));
      g.fillRect(startX, y, endX - startX, height);
    }
    
    // DRAW CONNECTIONS
    Graphics2D g2d = (Graphics2D) g.create();
    Stroke dashed = new BasicStroke(2, BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL, 0, null, 0);
    g2d.setStroke(dashed);
    int diameter = 8;
    if( node.previous != null ) {
      NodeView previousView = new NodeView(node.previous);

      int x0 = previousView.getMaxX();
      int x1 = getMinX();
      int y0 = getMidY() - 5;
      
      g2d.setColor(new Color(0,0,0,255));
      g2d.drawLine(x0, y0 , x1, y0);
      g2d.fillOval(x0 , y0 - diameter/2, diameter, diameter);

    }

    if( node.next != null ) {
      NodeView nextView = new NodeView(node.next);
      
      int x0 = getMaxX();
      int x1 = nextView.getMinX();
      int y0 = getMidY() + 5;
      
      g2d.setColor(new Color(0,0,0,255));
      g2d.drawLine(x0, y0 , x1, y0);
      g2d.fillOval(x1 - diameter, y0 - diameter/2, diameter, diameter);
    }
  }
}

