import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.util.*;

public class LinkedListDemoPanel extends JPanel implements MouseListener, MouseMotionListener, MouseWheelListener{
  
  RowContainerView container = new RowContainerView();
  Node focusedNode = null;

  public LinkedListDemoPanel() {
    addMouseListener(this);
    addMouseMotionListener(this);
    addMouseWheelListener(this);
  }
  
  public void reload() {
    container.reload();
    repaint();
  }
  
  //-
  public void mouseClicked(MouseEvent e) {
    repaint();
  }
  
  public void mousePressed(MouseEvent e) {
    
    if (e.getClickCount() == 1 && !e.isConsumed()) {
      e.consume();
      //handle double click event.
      
      if( focusedNode == null ) {
        
        CoordinateTransformer transformer = CoordinateTransformer.getInstance();
        
        int index = transformer.rowFromY(e.getY());
        if( index ==  0 ) {
          int data = transformer.dataFromX(e.getX());
          
          if( data != -1 ) {
            DataSource.getInstance().addNew(data);
            reload();
            System.out.println("** add new ** ");
          }
        }
        
      }else {
        System.out.println("** delete **");
        DataSource.getInstance().delete(focusedNode);
        focusedNode = null;
        
        reload();
        
      }
    }
    
    repaint();
  }
  
  public void mouseReleased(MouseEvent e) {
  }
  
  public void mouseExited(MouseEvent e) {
  }
  
  public void mouseEntered(MouseEvent e) {
  }
  
  public void mouseWheelMoved(MouseWheelEvent e) {
  }
  
  public void mouseMoved(MouseEvent e) {
    focusedNode = null;
    Node node = container.hitTest(e.getX(), e.getY());
    if( node != null) {
      focusedNode = node;
    }
    
    repaint();
  }
  
  
  public void mouseDragged(MouseEvent e) {
  }
  
  public void paintComponent(Graphics g) {
    g.setColor(Color.white);
    g.fillRect(0, 0, size().width, size().height);
    container.draw(g);
  }
}

