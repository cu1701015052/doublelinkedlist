import java.util.*;

public class CoordinateTransformer {
  private static final CoordinateTransformer instance = new CoordinateTransformer();

  public float viewY = 50;
  public final int rowHeight = 20;
  
  final float leftX = 20;
  final float rightX = 580;
  
  final int startData = 0;
  final int endData = 30;

  
  public static CoordinateTransformer getInstance(){
    return instance;
  }
  
  public int dataFromX(int intX) {
    if( intX < leftX ) { return -1; }
    if( intX > rightX ) { return -1; }

    float dx = (rightX - leftX) / (endData - startData + 1);
    float x = (float)intX;
    float data = startData + (x - leftX) / dx;
    if( data < 0 ) { return -1; }
    if( data > endData + 1) { return -1; }
    return (int)data;
  }

  
  public int xFromData(int intData) {

    float data = (float)intData;

    float dx = (rightX - leftX) / (endData - startData + 1);
    return (int)(leftX + (data - startData) * dx );
  }

  
  public int rowFromY(int y) {
    if( y >= viewY && y <= viewY + rowHeight ) {
      return 0;
    }
    
    return -1;
  }
}
