public class DataSource {
  private static final DataSource instance = new DataSource();

  DoublyLinkedList activeList = new DoublyLinkedList();
  
  public static DataSource getInstance() {
    return instance;
  }

  public void addNew(int data) {
    // MUST LOOK FOR THE APPROPRIATE LOCATION
    Node obj = new Node(data);
    
    Node p = activeList.top;
    int index = 0;
    while(p != null) {
      
      if( p.data > data ) {
        break;
      }
      
      p = p.next;
      index ++;
    }
    
    activeList.insert(obj, index);
    activeList.check();
  }
  
  public void delete(Node obj) {
    activeList.delete(obj);
    activeList.check();
    
  }
  
}
