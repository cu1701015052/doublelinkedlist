
public class DoublyLinkedList extends SinglyLinkedList {
  private Node last;

  public void add(Node next) {
    if( top == null ) {
      top = next;
      last = next;
      
      top.next = null;
      last.next = null;
      
      top.previous = null;
      last.previous = null;
      
      return;
    }
    
    if( last == top ) {
      last = next;
      top.next = last;
      last.previous = top;
      return;
    }
    last.next = next;
    next.previous = last;
    last = next;
    last.next = null;
  }
  
  public void set(Node node) {
    top = node;
    
    while( node.next != null ) {
      node = node.next;
    }
    last = node;
  }
  
  // ADD node AT index
  public boolean insert(Node addingNode, int index) {
    if( index < 0 ) { return false; }
    if( top == null ) {
      add(addingNode);
      return true;
    }
    
    if( index == 0 ) {
      addingNode.next = top;
      top.previous = addingNode;
      top = addingNode;
      return true;
    }
    
    Node node = getNodeAt(index-1);
    
    if( node == null || node.next == null) {
      add(addingNode);
      return true;
    }
    
    Node nextObject = node.next;
    addingNode.next = nextObject;
    nextObject.previous = addingNode;
    node.next = addingNode;
    addingNode.previous = node;
    return true;
  }
  
  // DELETE node AT index AND RETURN THE DELETED NODE
  public Node delete(int index) {
    if( index < 0 ) { return null; }
    
    if( index == 0 ) {
      Node deletedNode = top;
      top = top.next;
      top.previous = null;
      
      deletedNode.previous = null;
      deletedNode.next = null;

      return deletedNode;
    }
    
    Node node = getNodeAt(index-1);
    if( node == null || node.next == null ) {
      return null;
    }
    
    Node deletedNode = node.next;
    if( deletedNode.next == null ) {
      last = node;
      last.next = null;
    }else {
      node.next = deletedNode.next;
      node.next.previous = node;
    }
    
    deletedNode.previous = null;
    deletedNode.next = null;

    return deletedNode;
  }
  
  public Node deleteLast() {
    if( last == null ) { return null; }
    Node deletedNode = last;
    
    last.previous.next = null;
    last = last.previous;
    
    deletedNode.previous = null;
    deletedNode.next = null;

    return deletedNode;
  }
  
  public void delete(Node obj) {
    Node left = obj.previous;
    Node right = obj.next;

    if( left != null ) {
      left.next = right;
    }else {
      top = right;
    }
    
    if( right != null ) {
      right.previous = left;
    }else {
      last = left;
    }
    
    obj.previous = null;
    obj.next = null;
  }
  
  public boolean check() {
    if( top == null && last == null ){
      System.out.println("check: null");
      return true;
    }
    
    if( top == null && last != null ){
      System.out.println("check: top is null");
      return false;
    }
    
    if( top != null && last == null ){
      System.out.println("check: last is null");
      return false;
    }
    
    if( top == last ){
      System.out.println("check: single node");
      return true;
    }
    
    
    Node node = top;
    
    while( node.next != null ) {
      if( node != node.next.previous) {
        System.out.println("check: next and previous does to match");
        return false;
      }
      node = node.next;
    }
    
    if( node != last ) {
      System.out.println("check: last does to match");
      return false;
    }
    
    return true;
  }
  

}

