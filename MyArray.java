public class MyArray {
  
  Node[] array;
  private int arrayCount = 0;
  MyArray(Node[] array, int count) {
    this.array = array;
    arrayCount = count;
  }

  public int count() {
    return arrayCount;
  }


  // ADD node AT index
  public boolean insert(Node addingNode, int index) {
    if( index < 0 || index > arrayCount ) { return false; }
  
    Node[] bar = new Node[arrayCount+1];
    
    System.arraycopy(array, 0, bar, 0, index);
    System.arraycopy(array, index, bar, index+1, arrayCount-index);
    bar[index] = addingNode;
    array = bar;
    arrayCount++;

    return true;
  }
  
  public boolean add(Node addingNode) {
    int index = arrayCount;
    
    Node[] bar = new Node[arrayCount+1];
    
    System.arraycopy(array, 0, bar, 0, index);
    System.arraycopy(array, index, bar, index+1, arrayCount-index);
    bar[index] = addingNode;
    array = bar;
    arrayCount++;
    
    return true;
  }
  
  // DELETE Node AT index AND RETURN THE DELETED Node
  public Node delete(int index) {
    if( index < 0 || index >= arrayCount ) { return null; }

    Node[] bar = new Node[arrayCount-1];
    Node deletingNode = array[index];
    System.arraycopy(array, 0, bar, 0, index);
    System.arraycopy(array, index+1, bar, index, arrayCount-index-1);
    array = bar;
    arrayCount--;
    return deletingNode;
  }

}



