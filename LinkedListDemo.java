import java.awt.*;
import java.awt.event.*;
import java.awt.BorderLayout;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.util.*;


public class LinkedListDemo extends JFrame implements ActionListener {

  JTabbedPane tabbedpane;
  
  public LinkedListDemo() {

    setSize(600, 150);
    setResizable(false);
    
    setTitle("Seminar Theme 2");
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    LinkedListDemoPanel panel = new LinkedListDemoPanel();
    
    Container c = getContentPane();
    
    
    JButton button = new JButton("Test Performance");
    button.addActionListener(this);
    panel.add(button);
    
    c.add(panel);
    setVisible(true);
  }
  
  public void actionPerformed(ActionEvent e) {
    testPerformance();
    testSinglyLinkedList();
    testDoublyLinkedList();
    testSearch();
  }
  
  void testSinglyLinkedList() {
    
    SinglyLinkedList list = new SinglyLinkedList();
    Node one = new Node(1);
    list.add(one);
    
    Node two = new Node(2);
    list.add(two);
    
    Node three = new Node(3);
    list.add(three);
    
    list.delete(1);
    
    if( list.description().equals("1 -> 3") == false ){
      java.lang.System.exit(-1);
    }
    
    list.delete(0);
    
    if( list.description().equals("3") == false ){
      java.lang.System.exit(-1);
    }
    
    Node four = new Node(4);
    
    list.insert(four, 0);
    
    if( list.description().equals("4 -> 3") == false ){
      java.lang.System.exit(-1);
    }
    
    Node five = new Node(5);
    
    list.insert(five, 0);
    
    if( list.description().equals("5 -> 4 -> 3") == false ){
      java.lang.System.exit(-1);
    }
    
    list.delete(1);
    if( list.description().equals("5 -> 3") == false ){
      java.lang.System.exit(-1);
    }
  }
  
  void testDoublyLinkedList() {
    
    DoublyLinkedList doubleList = new DoublyLinkedList();
    Node one = new Node(1);
    Node two = new Node(2);
    Node three = new Node(3);
    Node four = new Node(4);
    
    doubleList.add(one);
    doubleList.add(two);
    if( doubleList.check() == false ){
      java.lang.System.exit(-1);
    }
    
    doubleList.insert(three, 0);
    doubleList.insert(four, 1);
    
    if( doubleList.check() == false ){
      java.lang.System.exit(-1);
    }
    
    doubleList.deleteLast();
    if( doubleList.check() == false ){
      java.lang.System.exit(-1);
    }
    
    doubleList.delete(1);
    if( doubleList.check() == false ){
      java.lang.System.exit(-1);
    }
  }
  
  void testSearch() {
    SinglyLinkedList list = new SinglyLinkedList();
    int numberOfNodes = 100;
    Node top = new Node(0);
    Node previous = top;
    for( int count = 0; count < numberOfNodes; count ++) {
      Node next = new Node(previous.data + 1);
      previous.next = next;
      previous = next;
    }
    list.set(top);

    int v = 51;
    Node foundNode = list.searchNodeByData(v);
    if( foundNode.data != v || foundNode != list.searchNodeByDataRecursively(v) ) {
      System.out.println("** testSearch failed  **");
      java.lang.System.exit(-1);
    }
    
    System.out.println("** testSearch done **");
  }
  
  void testPerformance() {
    
    // CREATE 100 NODES
    int numberOfNodes = 1000;
    int repeatCount = 5000000;
    
    // PREPARE SINGLE LINKED LIST
    SinglyLinkedList list = new SinglyLinkedList();
    Node top = new Node(0);
    list.set(top);

    for( int count = 0; count < numberOfNodes; count ++) {
      Node next = new Node(count + 1);
      list.add(next);
    }
    
    // PREPARE DOUBLE LINKED LIST
    DoublyLinkedList doubleList = new DoublyLinkedList();
    top = new Node(0);
    doubleList.set(top);
    
    for( int count = 0; count < numberOfNodes; count ++) {
      Node next = new Node(count + 1);
      doubleList.add(next);
    }
    
    //
    // [1] INSERTING AND DELETING AT THE BEGINNING OF THE LISTS
    //

    // REPEAT INSERTING AND DELETING
    System.out.println("** Inserting and deleting at the FIRST location **");
    
    long t0 = System.currentTimeMillis();
    
    // SINGLE LINKED LIST
    for( int hoge = 0; hoge < repeatCount; hoge++) {
      Node node = new Node();
      list.insert(node, 0);
      Node deletedNode = list.delete(0);
    }
    
    long t1 = System.currentTimeMillis();
    System.out.print("[SinglyLinkedList] Duration ");
    System.out.print( t1-t0 );
    System.out.print(" mSec\n");
    
    t0 = System.currentTimeMillis();
    
    // DOUBLE LINKED LIST
    for( int hoge = 0; hoge < repeatCount; hoge++) {
      // INSERT new Node
      Node node = new Node();
      doubleList.insert(node, 0);
      Node deletedNode = doubleList.delete(0);
    }
    
    t1 = System.currentTimeMillis();
    System.out.print("[DoublyLinkedList] Duration ");
    System.out.print( t1-t0 );
    System.out.print(" mSec\n");
    
    
//    // PREPARE MyArray
//    Node[] nodes = new Node[numberOfNodes];
//    for( int count = 0; count < numberOfNodes; count ++) {
//      Node next = new Node(count);
//      nodes[count] = next;
//    }
//    MyArray p1 = new MyArray(nodes, numberOfNodes);
//    
//    t0 = System.currentTimeMillis();
//    
//    for( int hoge = 0; hoge < repeatCount; hoge++) {
//      Node next = new Node();
//      p1.insert(next, 0);
//      p1.delete(0);
//    }
//    
//    t1 = System.currentTimeMillis();
//    System.out.print("[MyArray] Duration ");
//    System.out.print( t1-t0 );
//    System.out.print(" mSec\n");
    
    
    // JAVA ARRAY
    ArrayList<Node> nodesInArrayList = new ArrayList<Node>();
    for( int count = 0; count < numberOfNodes; count ++) {
      Node next = new Node(count);
      nodesInArrayList.add(next);
    }
    
    t0 = System.currentTimeMillis();
    
    for( int hoge = 0; hoge < repeatCount; hoge++) {
      Node next = new Node();
      nodesInArrayList.add(0, next);
      nodesInArrayList.remove(0);
    }
    
    t1 = System.currentTimeMillis();
    System.out.print("[ArrayList] Duration ");
    System.out.print( t1-t0 );
    System.out.print(" mSec\n");
    
    //
    // [2] INSERTING AND DELETING AT THE END OF THE LISTS
    //

    System.out.println("\n** Inserting and deleting at the LAST location **");
    
    t0 = System.currentTimeMillis();
    
    // SINGLE LINKED LIST
    for( int hoge = 0; hoge < repeatCount; hoge++) {
      // INSERT new Node
      Node node = new Node();
      list.add(node);
      Node deletedNode = list.delete(list.count()-1);
    }
    
    t1 = System.currentTimeMillis();
    System.out.print("[SinglyLinkedList] Duration ");
    System.out.print( t1-t0 );
    System.out.print(" mSec\n");
    
    t0 = System.currentTimeMillis();
    
    // DOUBLE LINKED LIST
    for( int hoge = 0; hoge < repeatCount; hoge++) {
      // INSERT new Node
      Node node = new Node();
      
      doubleList.add(node);
      Node deletedNode = doubleList.deleteLast();
    }
    
    t1 = System.currentTimeMillis();
    System.out.print("[DoublyLinkedList] Duration ");
    System.out.print( t1-t0 );
    System.out.print(" mSec\n");
    
//    t0 = System.currentTimeMillis();
//    
//    // MY ARRAY
//    for( int hoge = 0; hoge < repeatCount; hoge++) {
//      Node next = new Node();
//
//      p1.add(next);
//      p1.delete(p1.count()-1);
//    }
//    
//    t1 = System.currentTimeMillis();
//    System.out.print("[MyArray] Duration ");
//    System.out.print( t1-t0 );
//    System.out.print(" mSec\n");
//    
    
    t0 = System.currentTimeMillis();
    
    // JAVA ARRAY
    for( int hoge = 0; hoge < repeatCount; hoge++) {
      Node next = new Node();

      nodesInArrayList.add(next);
      nodesInArrayList.remove(nodesInArrayList.size()-1);
    }
    
    t1 = System.currentTimeMillis();
    System.out.print("[ArrayList] Duration ");
    System.out.print( t1-t0 );
    System.out.print(" mSec\n");
  }
}
